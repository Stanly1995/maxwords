﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxWords
{
    public class MaxWords : GiveInformation // inheritance from the superclass;
    {
        public String maxStr;
        public MaxWords(String str) : base (str) //constructor;
        {
            this.maxStr = str;
        }
        public String Max()
        {
            int MaxWords = 0;
            String finalStr = String.Empty;
            String[] strMax = ToStrArr();
            for (int i = 0; i < strMax.Length; i++) //definition of the maximum number of words;
            {
                if (NumOfWords(NewStr(strMax[i])) > MaxWords)
                {
                    MaxWords = NumOfWords(NewStr(strMax[i]));
                } 
            }
            for (int i = 0; i < strMax.Length; i++) // finding sentences with the maximum number of words;
            {
                if ((NumOfWords(NewStr(strMax[i])) == MaxWords)) {
                    finalStr = String.Concat(finalStr, NewStr(strMax[i]), someZn[i]," ");
                }
            }
            if (finalStr[finalStr.Length - 1] == 0) // removal of extra space;
                finalStr = finalStr.Remove(finalStr.Length - 1,1);
            finalStr = String.Concat("Предложени(е/я) с максимальным количеством слов: ", finalStr);
            return finalStr;

        }

        public override string ToString() //method ToString;
        {
            return Max();
        }
    }
}
