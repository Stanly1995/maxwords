﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaxWords
{
    public class GiveInformation //Super Class;
    {
        public String str;
        public List<char> someZn = new List<char>(); //List of punctuation marks;
        public GiveInformation(String str) //Constructor;
        {
            this.str = str;
        }
        public String[] ToStrArr() // Change to an array of strings;
        {
            String newSt = String.Empty;
            for (int i=0; i<str.Length; i++)
            {
                
                if (str[i]=='.' || str[i] == '?' || str[i] == '!')
                {
                    someZn.Add(str[i]);
                }
            }
            str = str.Replace('.', '|').Replace('!', '|').Replace('?', '|');
            return str.Split('|');
        }

        public int NumOfWords(String strLine) // count of words;
        {
            int numWords=0;
            for (int i = 0; i < strLine.Length; i++)
            {
                if (strLine[i] == ' ')
                {
                    numWords++;
                }
            }
            return numWords;
        }

        public String NewStr(String strLine) // removing extra spaces
        {
            for (int i = 1; i < strLine.Length; i++)
            {
                if (strLine[i] == ' ' && strLine[i - 1] == ' ')
                {
                    strLine = strLine.Remove(i, 1);
                    i--;
                }
            }
            if (strLine.Length > 0)
            {
                if (strLine[0] == ' ')
                {
                    strLine = strLine.Remove(0, 1);
                }
                if (strLine[strLine.Length - 1] == ' ')
                {
                    strLine = strLine.Remove(strLine.Length - 1, 1);
                }
            }
            return strLine;
        }
    }
}
